﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tether : MonoBehaviour {
    public LineRenderer tether;
    public GameObject shipOne;
    public GameObject shipTwo;
    public string enemy;
    
    void Start () {
		
	}
		
	void Update () {
        // raycast from ship one to two
        if (CheckShipDistance() && shipOne.activeSelf && shipTwo.activeSelf)
        {
            //if ships are close enough render line
            tether.enabled = true;
            tether.SetPosition(0, shipOne.transform.position);
            tether.SetPosition(1, shipTwo.transform.position);
            // check if line is hitting a ship
            Vector2 direction =  shipTwo.transform.position - shipOne.transform.position;
            float magnitute = Vector2.Distance(shipOne.transform.position, shipTwo.transform.position);
            RaycastHit2D hit = Physics2D.Raycast(shipOne.transform.position, direction, magnitute);
            Debug.DrawRay(shipOne.transform.position, direction);
            if (hit.collider != null && hit.collider.tag == enemy) {
                Debug.Log("hit");
                hit.collider.gameObject.GetComponent<ShipHealth>().Destroy();
            }

            // destroy ship is hit
        }
        else {
            tether.enabled = false;
        }        
    }

    bool CheckShipDistance() {
        if (Vector2.Distance(shipOne.transform.position, shipTwo.transform.position) < 5) {
            return true;
        }
        return false;
    }
}
