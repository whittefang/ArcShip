﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AreaType  {

    LeftRight,
    UpDown
}
