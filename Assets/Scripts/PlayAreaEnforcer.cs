﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAreaEnforcer : MonoBehaviour {
    public AreaType areaType;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D other) {
        if (areaType.Equals(AreaType.LeftRight)) {
            other.transform.position = new Vector2(-other.transform.position.x, other.transform.position.y);
        } else {
            other.transform.position = new Vector2(other.transform.position.x, -other.transform.position.y);
        }
    }
}
