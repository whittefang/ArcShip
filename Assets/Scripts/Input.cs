﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#

public class Input : MonoBehaviour
{
    public int  playerNumber;
    PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;

    ShipMover shipMover;

    // Use this for initialization
    void Start()
    {
        playerIndex = (PlayerIndex)playerNumber;
        shipMover = GetComponent<ShipMover>();
    }

    // Update is called once per frame
    void Update()
    {
        prevState = state;
        state = GamePad.GetState(playerIndex);

        if (Mathf.Abs(state.ThumbSticks.Left.X) > .5f || Mathf.Abs(state.ThumbSticks.Left.Y) > .5f)
        {
            // rotate ship
            shipMover.Rotate(new Vector2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y));
        }
        // move ship
        shipMover.Move(state.Triggers.Right);
    }
}
