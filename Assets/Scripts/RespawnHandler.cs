﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    }
    public void Resapwn(GameObject objectToRespawn, float respawnTime) {

        StartCoroutine(Respawn(objectToRespawn, respawnTime));
    }

    private IEnumerator Respawn(GameObject objectToRespawn, float duration) {
        yield return new WaitForSeconds(duration);
        objectToRespawn.SetActive(true);
    }
}
