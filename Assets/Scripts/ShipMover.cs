﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMover : MonoBehaviour {
    Rigidbody2D rigidbody2D;
    public float speed;
    public GameObject fireEffect;
    public ParticleSystem fireParticles;
    // Use this for initialization
    void Start(){
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        
    }   

    public void Move(float amount){
        if (rigidbody2D.velocity.sqrMagnitude > 20) {
            return;
        }
        if (amount > .05f)
        {
            fireEffect.SetActive(true);
            fireParticles.emissionRate = 20;

            Vector3 force = transform.up;
            force *= amount * speed;
            //Debug.Log(force + " " + amount);
            rigidbody2D.AddForce(force);
        }
        else {
            if (fireEffect.activeSelf)
            {
                fireParticles.emissionRate = 0;
                fireEffect.SetActive(false);
            }
        }

        
    }
    public void Rotate(Vector2 stickPosition) {
        float rot = Mathf.Atan2(-stickPosition.x, stickPosition.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0,0,rot);
    }
}
